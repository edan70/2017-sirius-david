# README# 

### LICENSE ###
This repository is covered by the license BSD 2-clause, see file LICENSE.

### Credits ###
A number of information was found on the Sirius official forum 
https://www.eclipse.org/forums/index.php?t=thread&frm_id=262

This README contains necessary steps to get your application up and running.

### To install the editor ###
1. Download the updateSiteProject folder and unzip it somewhere.
1. In Obeodesigner, go to Help->Install New Software
1. Select Add->Local. Then browse to the extracted updateSiteProject (the features and plugins folders must be in there)
1. New items should now appear in the list. Select all the items and keep clicking next to install them (accept the license or security warning when promted)
1. Restart Eclipse

The application is now ready to be used

### To import the example projects ###
1. Download the runtime_endUser folder and unzip it somewhere
1. In Obedesigner, go to File->Import->Existing projects into workspace. Browse to the extracted folder.
1. In the appeared list, select empty and example. (Check copy projects into workspace if needed). Then Finish

The example project contains an example while the empty project is already setup for a quick start.

### To start a new project ###
1. Go to File->New->Other. Select Modelling Project under Sirius. Write in a name then click on Finish.
1. In Model Explorer, right click on the project created in step 1 ->New->Other. Select Statechart Model under Example EMF Model Creation Wizards. Write in a name, then select Program under Model object. Then click on Finish.
1. In Model Explorer, right click the project created in step 1 again->Viewpoint Selection->Default viewpoint
1. Expand the .statechart file created in step 2->Right click on Program->New representation->New Program diagram

### To import the development folders ###
1. Download the 3 edan70.* folders, the runtime_dev and runtime_endUser folders. Then unzip them somewhere
1. Start obedesigner and import the 3 edan70.* folders as existing projects like above.
1. Right click edan70.statechart project and select Run As -> Eclipse Application
1. In the new Eclipse instance, import the runtime_dev folder as existing project
1. Right click the imported project and select Run As -> Eclipse Application again
1. In the third Eclipse instance, import runtime_endUser as existing projects.

### To run the example project ###
1. Expand the example.statechart and then double click on Program editor to open the main editor.
(If it doesnt work, you might have to do this first: In Model Explorer, right click the example project->Viewpoint Selection->Default viewpoint)
1. Right click on an empty area -> My Context Menu -> Run/Stop
1. Click on the events in the property view to trigger the state transitions. (If you dont see the list, you can try click on an empty area)
1. Repeat step 3 to stop the simulation

### To use the editor ###
1. The tools to create the elements are on the right side, 
on the bottom is the property view which can be used to edit the model.
If you dont see the edit options, you can try selecting the semantic tab on the left side of the property view.
1. Some elements can be tricky to create: events and types must be created by first selecting the tool and then click on the top
of the program box (the yellow box). To add states to an AND state, you also have to click on the top of the AND state
1. To open a type editor, first create a type then double click on the type name
1. To edit the name of states, events or types, click on it to select it then type the name (the marker usually appear after a few seconds but you dont have to wait for it)
1. To assign an event to a newly created edge, select the edge and type in the name of the event
1. There are two types of type instances: TypeInstance make new copies of the states, TypeInstanceShare just create new graphical views.
Only TypeInstance works with the simulation. Most operations involving TypeInstanceShare have been blocked except edge reconnection (due to a bug in Sirius),
and the property view which cannot be blocked.
1. To run the simulation, the program and each xor_state, must have a start state first. This can be done by right click on the state->My context menu-> Set start
If the simulation doesnt run, it is likely some start states have not been set (the script might have missed some cases). 
This can be checked in the semantic tab in the property view and edited manually there.
AND states dont need start states, they always start all children states.



### Trouble shooting ####
* In case the new runtime environment contains error, try go to the first environment->Window->Preferences->Plugin development->Target platform. Add and select an approriate platform

### Known issues ###
* The icons for some of the tools and the history state are missing because the plugin export process did not include them.
