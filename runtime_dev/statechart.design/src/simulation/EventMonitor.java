package simulation;

import java.util.Collection;
import java.util.Iterator;
import java.util.Scanner;

import edan70.statechart.Event;

public class EventMonitor {
	private final int size;
	private Event[] pendingEvents;
	private int nEvents;
	private int nextToFetch, nextToPut;
	public EventMonitor() {
		// TODO Auto-generated constructor stub
		size = 20;
		pendingEvents = new Event[size];
	}
	
	public synchronized void tryPutEvent(Event event) {
		if (nEvents == size) return;
		pendingEvents[nextToPut] = event;
		nEvents++;
		if (++nextToPut == size) nextToPut = 0;
		notifyAll();
	}
	
	public synchronized Event fetchEvent() throws InterruptedException {
		while (nEvents == 0) wait();
		Event e = pendingEvents[nextToFetch];
		pendingEvents[nextToFetch] = null;
		nEvents--;
		if (++nextToFetch == size) nextToFetch = 0;
		notifyAll();
		return e;
	}
}
