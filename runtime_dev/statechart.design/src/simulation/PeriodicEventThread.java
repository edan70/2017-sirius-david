package simulation;

import edan70.statechart.Event;
import edan70.statechart.PeriodicEvent;
import statechart.design.GlobalState;

public class PeriodicEventThread extends Thread {
	private EventMonitor mon;
	private PeriodicEvent pEvent;

	public PeriodicEventThread(EventMonitor mon, PeriodicEvent pEvent) {
		super();
		this.mon = mon;
		this.pEvent = pEvent;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			while (!currentThread().isInterrupted()) {
				Thread.sleep(pEvent.getPeriod() * 1000);
				mon.tryPutEvent(pEvent);
			}
		} catch (InterruptedException e) {
			System.out.println("Periodic thread " + pEvent.getName() + " interrupted");
		}
	}
}
