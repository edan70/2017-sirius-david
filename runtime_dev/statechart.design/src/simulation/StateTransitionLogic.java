package simulation;

import java.util.ArrayList;
import java.util.List;

import edan70.statechart.AND_State;
import edan70.statechart.Event;
import edan70.statechart.HistoryState;
import edan70.statechart.SimpleState;
import edan70.statechart.State;
import edan70.statechart.StateJump;
import edan70.statechart.TypeInstanceShare;
import edan70.statechart.XOR_State;
import statechart.design.GlobalState;

public class StateTransitionLogic {
	
	public static void handleExiting(State state, Event event, SiriusHelper helper, List<StateJump> stateJumps) {
		if (!state.isSimulating()) return;
		boolean jumped = false;
		for (StateJump sj : state.getStatejumps()) {
			if (sj.getEvents().contains(event)) {
				helper.setSimulatingState(state, false);
				turnOffSimulatingChildren(state, helper);
				turnOffSimulatingParents(state, helper);
				helper.setSimulatingStateJump(sj, true);
				stateJumps.add(sj);
				jumped = true;
			}
		}
		if (!jumped) {
			for (State s : state.getStates()) {
				handleExiting(s, event, helper, stateJumps);
			}
		}
		
	}
	
	public static void handleEntering(List<StateJump> stateJumps, SiriusHelper helper) {
		for (StateJump sj : stateJumps) {
			enterState(sj.getSecond(), helper);
		}
	}
	
	/**
	 * Recursively add the state's parents to the list of statesTobeTurnedOff.
	 * @param state
	 */
	private static void turnOffSimulatingParents(State state,SiriusHelper helper) {
		State p = state.getParent();
		while (p != null) {
			if (p instanceof AND_State) {
				for (State s : p.getStates()) {
					if (s.isSimulating() && s != state)	break;
				}
			}
			helper.setSimulatingState(p, false);
			p = p.getParent();
		}
	}
	
	/**
	 * Recursively add state's children to the list statesTobeTurnedOff.
	 * @param state
	 */
	private static void turnOffSimulatingChildren(State state, SiriusHelper helper) {
		if (state.getStates() != null && !state.getStates().isEmpty()) {
			for (State child : state.getStates()) {
				if (child.isSimulating()) {
					helper.setSimulatingState(child, false);
					turnOffSimulatingChildren(child, helper);
				}
			}
		}
	}
	
	/**
	 * Recursively add the state's parents to the list of statesTobeTurnedOn.
	 * @param state
	 */
	private static void turnOnSimulatingParents(State state, SiriusHelper helper) {
		State p = state.getParent();
		while (p != null) {
			if (p.isSimulating()) break;
			helper.setSimulatingState(p, true);
			p = p.getParent();
		}
	}
	
	/**
	 * Recursively add state's children to the list statesTobeTurnedOn.
	 * @param state
	 */
	private static void turnOnSimulatingChildren(State state, SiriusHelper helper) {
		if (state.getStates() != null && !state.getStates().isEmpty()) {
			for (State child : state.getStates()) {
				if (child.isSimulating()) {
					helper.setSimulatingState(child, true);
					turnOnSimulatingChildren(child, helper);
				}
			}
		}
	}
	
	public static void enterState(State state, SiriusHelper helper) {
		if (state == null) {
			GlobalState.INSTANCE().endSimulation();
			return;
		}
		helper.setSimulatingState(state, true);
		turnOnSimulatingParents(state, helper);
		
		if (state instanceof SimpleState) {
			enterSimpleState((SimpleState)state, helper);
		}else if (state instanceof XOR_State) {
			enterXORState((XOR_State)state, helper);
		}else if (state instanceof AND_State) {
			enterANDState((AND_State)state, helper);
		}else if (state instanceof HistoryState) {
			enterHistoryState((HistoryState)state, helper);
		}
	}
	
	private static void enterSimpleState(SimpleState state, SiriusHelper helper ) {
		//update history states
		State p = state.getParent();
		State s = state;
		while (p != null) {
			if (p instanceof XOR_State) {
				XOR_State xp = (XOR_State)p;
				if (xp.getHistoryState() != null) {
					helper.setHistoryState_LastState(xp.getHistoryState(), s);
				}
			}
			s = p;
			p = p.getParent();
		}
	}
	
	private static void enterXORState(XOR_State state, SiriusHelper helper) {
		if (state instanceof TypeInstanceShare) {
			//this if statement is a quick fix
			TypeInstanceShare t = (TypeInstanceShare)state;
			for (State s : t.getReferencedType().getStates()) {
				if (s.isStart()) {
					enterState(s,helper);
					return;
				}
			}
			enterState(null, helper);
			return;
		} 
		
		HistoryState historyState = state.getHistoryState();
		if (historyState != null && historyState.getLastState() != null) {
			enterState(historyState.getLastState(), helper);
		}else {
			enterState(state.getStartState(), helper);
		}
	}
	
	private static void enterANDState(AND_State state, SiriusHelper helper) {
		for (State s : state.getStates()) {
			enterState(s, helper);
		}
	}
	
	private static void enterHistoryState(HistoryState state, SiriusHelper helper) {
		enterState(state.getLastState(), helper);
	}
}
