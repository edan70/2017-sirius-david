package simulation;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.sirius.business.api.session.Session;

import edan70.statechart.AND_State;
import edan70.statechart.Event;
import edan70.statechart.PeriodicEvent;
import edan70.statechart.Program;
import edan70.statechart.SimpleState;
import edan70.statechart.State;
import edan70.statechart.StateJump;
import edan70.statechart.Type;
import edan70.statechart.XOR_State;

public class Simulation extends Thread {
	private Program program;
	private EventMonitor eventMonitor;
	private SiriusHelper siriusHelper;
	private List<PeriodicEventThread> periodicEventThreads;

	public Simulation(Program program, Session session) {
		// TODO Auto-generated constructor stub
		this.program = program;
		eventMonitor = new EventMonitor();
		siriusHelper = new SiriusHelper(session);
		periodicEventThreads = new ArrayList<>();
	}

	@Override
	public void run() {
		for (Event e : program.getEvents()) {
			if (e instanceof PeriodicEvent) {
				PeriodicEventThread t = new PeriodicEventThread(eventMonitor, (PeriodicEvent)e);
				periodicEventThreads.add(t);
				t.start();
			}
		}
		
		//turn on the start states
		StateTransitionLogic.enterState(program.getStartState(), siriusHelper);
		while (!currentThread().isInterrupted()) {
			Event e = null;
			try {
				e = eventMonitor.fetchEvent();
				//turn off current states and turn on approriate state jumps
				ArrayList<StateJump> stateJumps = new ArrayList<>();
				for (edan70.statechart.State s : program.getStates()) {
					StateTransitionLogic.handleExiting(s, e, siriusHelper, stateJumps);
				}
				
				//after a short delay, turn off state jumps and turn on new states
				if (!stateJumps.isEmpty()) Thread.sleep(200);
				for (StateJump sj : stateJumps) {
					siriusHelper.setSimulatingStateJump(sj, false);
				}
				StateTransitionLogic.handleEntering(stateJumps, siriusHelper);
			} catch (InterruptedException exc) {
				// TODO Auto-generated catch block
				System.out.println("Main simulation thread interrupted");
				break;
			}
		}
	}
	
	public void interruptSimulationThreads() {
		this.interrupt();
		for (PeriodicEventThread t : periodicEventThreads) {
			t.interrupt();
		}
	}

	public void reset() {
		// TODO Auto-generated method stub
		for (edan70.statechart.State s : program.getStates()) {
			turnOffSimulating(s);
		}
		
		for (Type t : program.getTypes()) {
			for (edan70.statechart.State s : t.getStates()) {
				turnOffSimulating(s);
			}
		}
	}
	
	private void turnOffSimulating(edan70.statechart.State state) {
		siriusHelper.setSimulatingState(state, false);
		if (state instanceof XOR_State) {
			XOR_State xs = (XOR_State)state;
			if (xs.getHistoryState() != null) {
				siriusHelper.setHistoryState_LastState(xs.getHistoryState(), xs.getStartState());
			}
		}
		for (StateJump sj : state.getStatejumps()) {
			siriusHelper.setSimulatingStateJump(sj, false);
		}
		if (state.getStates() != null) {
			for (edan70.statechart.State s : state.getStates()) {
				turnOffSimulating(s);
			}
		}
	}
	
	
	public void print() {
		// TODO Auto-generated method stub
		for (edan70.statechart.State s : program.getStates()) {
			printSimulating(s);
		}
	}
	
	private void printSimulating(edan70.statechart.State state) {
		System.out.println("State " + state.getName() + ": " + state.isSimulating());
		if (state.getStates() != null) {
			for (edan70.statechart.State s : state.getStates()) {
				printSimulating(s);
			}
		}
	}

	public EventMonitor getEventMonitor() {
		return eventMonitor;
	}
}
