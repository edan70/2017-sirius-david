package simulation;

import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.MoveCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.transaction.NotificationFilter;
import org.eclipse.emf.transaction.ResourceSetChangeEvent;
import org.eclipse.emf.transaction.ResourceSetListener;
import org.eclipse.emf.transaction.ResourceSetListenerImpl;
import org.eclipse.emf.transaction.RollbackException;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.sirius.business.api.dialect.DialectManager;
import org.eclipse.sirius.business.api.dialect.command.RefreshRepresentationsCommand;
import org.eclipse.sirius.business.api.session.Session;

import edan70.statechart.HistoryState;
import edan70.statechart.SimpleState;
import edan70.statechart.State;
import edan70.statechart.StateJump;
import edan70.statechart.StatechartPackage;
import edan70.statechart.TypeInstance;

public class SiriusHelper {

	private Session session;
	private TransactionalEditingDomain domain;
	private CommandStack stack;

	public SiriusHelper(Session session) {
		// TODO Auto-generated constructor stub
		this.session = session;
		domain = session.getTransactionalEditingDomain();
		stack = domain.getCommandStack();
		// CommandStack stack = domain.getCommandStack();
		// for(DView view : session.getOwnedViews()){
		// RefreshRepresentationsCommand cmd = new RefreshRepresentationsCommand(domain,
		// null, view.getOwnedRepresentations());
		// domain.getCommandStack().execute(cmd);
		// }
	}

	public void refreshEditor() {
		RefreshRepresentationsCommand cmd = new RefreshRepresentationsCommand(domain, null,
				DialectManager.INSTANCE.getAllRepresentations(session));
		stack.execute(cmd);
	}

	public void setSimulatingState(State state, boolean value) {
		Command cmd = domain.createCommand(SetCommand.class,
				new CommandParameter(state, StatechartPackage.Literals.STATE__SIMULATING, value));
		stack.execute(cmd);
	}

	public void setSimulatingStateJump(StateJump stateJump, boolean value) {
		Command cmd = domain.createCommand(SetCommand.class,
				new CommandParameter(stateJump, StatechartPackage.Literals.STATE_JUMP__SIMULATING, value));
		stack.execute(cmd);
	}

	public synchronized void setModifiedTypeInstance(TypeInstance typeInstance, boolean value) {
		Command cmd = domain.createCommand(SetCommand.class,
				new CommandParameter(typeInstance, StatechartPackage.Literals.TYPE_INSTANCE__MODIFIED, value));
		stack.execute(cmd);
	}

	public synchronized void setName(State typeInstance, String value) {
		Command cmd = domain.createCommand(SetCommand.class,
				new CommandParameter(typeInstance, StatechartPackage.Literals.STATE__NAME, value));
		stack.execute(cmd);
	}

	public synchronized void setStart(State typeInstance, String value) {
		Command cmd = domain.createCommand(SetCommand.class,
				new CommandParameter(typeInstance, StatechartPackage.Literals.STATE__START, value));
		stack.execute(cmd);
	}

	public synchronized void setStateJumpFirst(StateJump typeInstance, State value) {
		State oldState = typeInstance.getFirst();
		Command cmd = domain.createCommand(SetCommand.class,
				new CommandParameter(typeInstance, StatechartPackage.Literals.STATE_JUMP__FIRST, value));
		stack.execute(cmd);
		AddCommand add = new AddCommand(domain, value, StatechartPackage.Literals.STATE__STATEJUMPS, typeInstance);
		RemoveCommand c = new RemoveCommand(domain, oldState, StatechartPackage.Literals.STATE__STATEJUMPS, typeInstance);
		add.execute();
		c.execute();
	}
	
	public synchronized void setHistoryState_LastState(HistoryState historyState, State value) {
		Command cmd = domain.createCommand(SetCommand.class,
				new CommandParameter(historyState, StatechartPackage.Literals.HISTORY_STATE__LAST_STATE, value));
		stack.execute(cmd);
	}

	public void addResourceSetListener(ResourceSetListener listener) {
		domain.addResourceSetListener(listener);
	}

	public void test() {
		domain.addResourceSetListener(new MyListener());
	}

	class MyListener extends ResourceSetListenerImpl {
		public void resourceSetChanged(ResourceSetChangeEvent event) {
			System.out.println("--------------");
			System.out.println(event.getEditingDomain() == domain);
			System.out.println("Domain " + event.getEditingDomain().getID() + " changed "
					+ event.getNotifications().size() + " times");
			List<Notification> nos = event.getNotifications();
			for (Notification n : nos) {
				// System.out.println(n.getNotifier());
			}
		}
	}

}
