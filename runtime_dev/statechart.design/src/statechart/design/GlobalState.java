package statechart.design;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import edan70.statechart.TypeInstance;
import simulation.EventMonitor;
import simulation.Simulation;

public class GlobalState {
	private static GlobalState _instance = null;
	
	public static GlobalState INSTANCE() {
		if (_instance == null) _instance = new GlobalState();
		return _instance;
	}
	
	private Simulation sim;
	private Set<TypeInstance> modifiedTypeInstances;
	private Set<TypeInstance> createdTypeInstances;
	
	public GlobalState() {
		// TODO Auto-generated constructor stub
		modifiedTypeInstances = new HashSet<>();
		createdTypeInstances = new HashSet<>();
	}
	
	public void endSimulation() {
		System.out.println("Ending simulation");
		sim.interruptSimulationThreads();
		sim.reset();
		sim = null;
	}

	public Simulation getSim() {
		return sim;
	}

	public void setSim(Simulation sim) {
		this.sim = sim;
	}
	
	public EventMonitor getEventMonitor() {
		return sim.getEventMonitor();
	}
	
	public void addCreatedTypeInstance(TypeInstance t) {
		createdTypeInstances.add(t);
	}
	
	public boolean isCreated(TypeInstance t) {
		return createdTypeInstances.contains(t);
	}
	
	public void addModifiedTypeInstance(TypeInstance t) {
		modifiedTypeInstances.add(t);
	}
	
	public boolean isModified(TypeInstance t) {
		return modifiedTypeInstances.contains(t);
	}
	
}
