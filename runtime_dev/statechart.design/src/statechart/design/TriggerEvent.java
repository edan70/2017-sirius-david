package statechart.design;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.sirius.tools.api.ui.IExternalJavaAction;

import edan70.statechart.Event;

public class TriggerEvent implements IExternalJavaAction {

	public TriggerEvent() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute(Collection<? extends EObject> selections, Map<String, Object> parameters) {
		// TODO Auto-generated method stub
		Event event = (Event) parameters.get("selected");
		GlobalState.INSTANCE().getEventMonitor().tryPutEvent(event);
	}

	@Override
	public boolean canExecute(Collection<? extends EObject> selections) {
		// TODO Auto-generated method stub
		return true;
	}

}
