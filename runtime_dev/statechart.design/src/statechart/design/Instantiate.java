package statechart.design;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.ResourceSetChangeEvent;
import org.eclipse.emf.transaction.ResourceSetListener;
import org.eclipse.emf.transaction.ResourceSetListenerImpl;
import org.eclipse.emf.transaction.RollbackException;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.SessionManager;
import org.eclipse.sirius.tools.api.ui.IExternalJavaAction;

import edan70.statechart.Pair;
import edan70.statechart.State;
import edan70.statechart.StateJump;
import edan70.statechart.StatechartFactory;
import edan70.statechart.StatechartPackage;
import edan70.statechart.Type;
import edan70.statechart.TypeInstance;
import edan70.statechart.util.StatechartAdapterFactory;
import simulation.SiriusHelper;

public class Instantiate implements IExternalJavaAction {

	private static ResourceSetListener listener = null;

	public Instantiate() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute(Collection<? extends EObject> selections, Map<String, Object> parameters) {
		// TODO Auto-generated method stub

		TypeInstance instance = (TypeInstance) parameters.get("typeInstance");
		Session session = SessionManager.INSTANCE.getSession(instance);
		SiriusHelper helper = new SiriusHelper(session);
//		if (listener == null) {
//			listener = new MyListener(helper);
//			helper.addResourceSetListener(listener);
//		}

		Type referencedType = instance.getReferencedType();
		referencedType.getInstances().add(instance);
		for (State s : referencedType.getStates()) {
			createStates(s, instance, instance);
		}
		
		for (State s : referencedType.getStates()) {
			createStateJumps(s, instance);
		}
		
		for (State s : instance.getStates()) {
			if (s.isStart()) {
				instance.setStartState(s);
				break;
			}
		}
	}
	
	private void createStates(State originalState, State parent, TypeInstance instance) {
		State newState = cloneState(originalState, parent);
		for (State s : originalState.getStates()) {
			createStates(s, newState, instance);
		}
		StatechartFactory factory = StatechartFactory.eINSTANCE;
		Pair pair = factory.createPair();
		pair.setTypeinstance(instance);
		pair.setState(newState);
		originalState.getClonedStates().add(pair);
		parent.getStates().add(newState);
	}

	private State cloneState(State state, State parent) {
		StatechartFactory factory = StatechartFactory.eINSTANCE;
		State newState = (State) factory.create(state.eClass());
		newState.setName(state.getName());
		newState.setSimulating(state.isSimulating());
		newState.setStart(state.isStart());
		
		newState.setParent(parent);
		
		return newState;
	}
	
	private void createStateJumps(State originalState, TypeInstance instance) {
		State clone = getClone(originalState, instance);
		for (StateJump sj : originalState.getStatejumps()) {
			StateJump newSj = cloneStateJump(sj, instance);
			clone.getStatejumps().add(newSj);
		}
		for (State s : originalState.getStates()) {
			createStateJumps(s, instance);
		}
	}
	
	private StateJump cloneStateJump(StateJump original, TypeInstance instance) {
		StatechartFactory factory = StatechartFactory.eINSTANCE;
		StateJump newSj = factory.createStateJump();
		newSj.getEvents().addAll(original.getEvents());
		State clonedFirst = getClone(original.getFirst(), instance);
		State clonedSecond = getClone(original.getSecond(), instance);
		newSj.setFirst(clonedFirst);
		newSj.setSecond(clonedSecond);
		return newSj;
	}
	
	private State getClone(State original, TypeInstance instance) {
		for (Pair p : original.getClonedStates()) {
			if (p.getTypeinstance() == instance) {
				return p.getState();
			}
		}
		return null;
	}

	@Override
	public boolean canExecute(Collection<? extends EObject> selections) {
		// TODO Auto-generated method stub
		return true;
	}

//	class MyListener extends ResourceSetListenerImpl {
//
//		private SiriusHelper siriusHelper;
//
//		public MyListener(SiriusHelper siriusHelper) {
//			super();
//			this.siriusHelper = siriusHelper;
//		}
//
//		public void resourceSetChanged(ResourceSetChangeEvent event) {
//			List<Notification> nos = event.getNotifications();
//			for (Notification n : nos) {
//				Object notifier = n.getNotifier();
//				if (notifier instanceof State || notifier instanceof StateJump) {
//					TypeInstance t = findTypeInstance((EObject) notifier);
//					Type type = findType((EObject) notifier);
//					if (t != null) {
//						// setting the modified flag in the type instance semantic object here resulted
//						// in a
//						// "Cannot activate read/write transaction in read-only transaction context"
//						// exception.
//						// So globalState is used as a trick to go around this
//						/*if (GlobalState.INSTANCE().isCreated(t)) {
//							Thread thread = new Thread() {
//								public void run() {
//									siriusHelper.setModifiedTypeInstance(t, true);
//								}
//
//							};
//							thread.start();
//						} else {
//							GlobalState.INSTANCE().addCreatedTypeInstance(t);
//							break;
//						}*/
//
//					} else if (type != null) {
//						Thread thread = new Thread() {
//							public void run() {
//								applyChange(n, siriusHelper);
//							}
//						};
//						thread.start();
//
//					}
//				}
//			}
//		}
//	}
//
//	private TypeInstance findTypeInstance(EObject eObj) {
//		EObject container = eObj.eContainer();
//		while (container != null && !(container instanceof TypeInstance)) {
//			container = container.eContainer();
//		}
//		return (TypeInstance) container;
//	}
//
//	private Type findType(EObject eObj) {
//		EObject container = eObj.eContainer();
//		while (container != null && !(container instanceof Type)) {
//			container = container.eContainer();
//		}
//		return (Type) container;
//	}
//
//	private void applyChange(Notification n, SiriusHelper helper) {
//		if (n.getNotifier() instanceof State) {
//			State state = (State) n.getNotifier();
//			int featureID = n.getFeatureID(StatechartPackage.class);
//			switch (featureID) {
//			case StatechartPackage.STATE__NAME:
//				for (State r : state.getReferencingStates()) {
//					helper.setName(r, n.getNewStringValue());
//				}
//				break;
//			case StatechartPackage.STATE__START:
//				for (State r : state.getReferencingStates()) {
//					helper.setStart(r, n.getNewStringValue());
//				}
//				break;
//			default:
//				break;
//			}
//		}else if (n.getNotifier() instanceof StateJump) {
//			StateJump sj = (StateJump)n.getNotifier();
//			int featureID = n.getFeatureID(StatechartPackage.class);
//			switch (featureID) {
//			case StatechartPackage.STATE_JUMP__FIRST:
//				
//				break;
//
//			default:
//				break;
//			}
//		}
//	}

}
