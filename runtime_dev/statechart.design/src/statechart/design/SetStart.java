package statechart.design;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.sirius.tools.api.ui.IExternalJavaAction;

import edan70.statechart.AND_State;
import edan70.statechart.Program;
import edan70.statechart.State;
import edan70.statechart.Type;
import edan70.statechart.XOR_State;

public class SetStart implements IExternalJavaAction {
	
	private State topState;

	public SetStart() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute(Collection<? extends EObject> selections, Map<String, Object> parameters) {
		// TODO Auto-generated method stub
		State state = (State)selections.iterator().next();
		if (state.isStart()) return;
		
		State p = state.getParent();
		state.setStart(true);
		if (p != null) {
			if (p instanceof XOR_State) {
				XOR_State t = (XOR_State)p;
				if (t.getStartState() != null) t.getStartState().setStart(false);
				t.setStartState(state);
				if (t.getHistoryState() != null) t.getHistoryState().setLastState(state);
			}
		}else if (state.eContainer() instanceof Program){
			Program prog = (Program) state.eContainer();
			if (prog.getStartState() != null) prog.getStartState().setStart(false);
			prog.setStartState(state);
		}else if (state.eContainer() instanceof Type) {
			Type type = (Type)state.eContainer();
			for (State s : type.getStates()) {
				if (s != state) {
					s.setStart(false);
				}
			}
		}
	}

	@Override
	public boolean canExecute(Collection<? extends EObject> selections) {
		// TODO Auto-generated method stub
		return true;
	}

}
