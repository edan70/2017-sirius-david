package statechart.design;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.SessionManager;
import org.eclipse.sirius.tools.api.ui.IExternalJavaAction;

import edan70.statechart.Program;
import edan70.statechart.TypeInstance;
import simulation.SiriusHelper;

public class Test implements IExternalJavaAction {

	public Test() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute(Collection<? extends EObject> selections, Map<String, Object> parameters) {
		// TODO Auto-generated method stub
		Program program =  (Program) selections.iterator().next();
		Session session = SessionManager.INSTANCE.getSession(program);
		SiriusHelper helper = new SiriusHelper(session);
		helper.test();
	}

	@Override
	public boolean canExecute(Collection<? extends EObject> selections) {
		// TODO Auto-generated method stub
		return true;
	}

}
