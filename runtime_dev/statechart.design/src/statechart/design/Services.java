package statechart.design;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import edan70.statechart.AND_State;
import edan70.statechart.State;

/**
 * The services class used by VSM.
 */
public class Services {
    
    /**
    * See http://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.sirius.doc%2Fdoc%2Findex.html&cp=24 for documentation on how to write service methods.
    */
    public Boolean checkANDStateconditions(EObject preSource, EObject preTarget) {
      EObject sourceContainer0 = preSource;
      EObject sourceContainer1 = preSource.eContainer();
      EObject targetContainer0 = preTarget;
      EObject targetContainer1 = preTarget.eContainer();
      
      while (sourceContainer1 != null && !(sourceContainer1 instanceof AND_State)) {
    	  sourceContainer0 = sourceContainer1;
    	  sourceContainer1 = sourceContainer1.eContainer();
      }
      
      while (targetContainer1 != null && !(targetContainer1 instanceof AND_State)) {
    	  targetContainer0 = targetContainer1;
    	  targetContainer1 = targetContainer1.eContainer();
      }
      
      if (sourceContainer1 == null && targetContainer1 == null) {
    	  return true;
      }else if (sourceContainer1 == targetContainer1 && sourceContainer0 == targetContainer0) {
    	  return true;
      }
      
      return false;
    }
}
