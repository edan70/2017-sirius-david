package statechart.design;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.SessionManager;
import org.eclipse.sirius.tools.api.ui.IExternalJavaAction;

import edan70.statechart.Program;
import simulation.Simulation;

public class Simulate implements IExternalJavaAction{

	private Simulation sim;
	
	public Simulate() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute(Collection<? extends EObject> selections, Map<String, Object> parameters) {
		// TODO Auto-generated method stub
		if (GlobalState.INSTANCE().getSim() == null) {
			System.out.println("Starting new simulation");
			Program program = (Program)selections.iterator().next();
			Session session = SessionManager.INSTANCE.getSession(program); // the semantic EObject
			sim = new Simulation(program, session);
			GlobalState.INSTANCE().setSim(sim);
			sim.start();
		}else {
			System.out.println("\nTerminating simulation");
			GlobalState.INSTANCE().endSimulation();
		}
		
	}

	@Override
	public boolean canExecute(Collection<? extends EObject> selections) {
		// TODO Auto-generated method stub
		return true;
	}

}
