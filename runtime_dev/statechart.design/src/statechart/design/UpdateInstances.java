package statechart.design;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.sirius.tools.api.ui.IExternalJavaAction;

import edan70.statechart.Pair;
import edan70.statechart.State;
import edan70.statechart.StateJump;
import edan70.statechart.Type;
import edan70.statechart.TypeInstance;

public class UpdateInstances implements IExternalJavaAction {

	public UpdateInstances() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute(Collection<? extends EObject> selections, Map<String, Object> parameters) {
		// TODO Auto-generated method stub
		Type type = (Type)selections.iterator().next();
		for (TypeInstance i : type.getInstances()) {
			for (State s : type.getStates()) {
				updateState(s, i);
			}
		}
		State startState = null;
		for (State s : type.getStates()) {
			if (s.isStart()) {
				startState = s;
				break;
			}
		}
		if (startState != null) {
			for (TypeInstance i : type.getInstances()) {
				i.setStartState(getClone(startState, i));
			}
		}
	}
	
	private void updateState(State original, TypeInstance instance) {
		State clone = getClone(original, instance);
		if (clone != null) {
			clone.setName(original.getName());
			clone.setSimulating(original.isSimulating());
			clone.setStart(original.isStart());
		}
		for (State s : original.getStates()) {
			updateState(s, instance);
		}
	}
	
	private State getClone(State original, TypeInstance instance) {
		for (Pair p : original.getClonedStates()) {
			if (p.getTypeinstance() == instance) {
				return p.getState();
			}
		}
		return null;
	}

	@Override
	public boolean canExecute(Collection<? extends EObject> selections) {
		// TODO Auto-generated method stub
		return true;
	}

}
