/**
 */
package edan70.statechart;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>History State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edan70.statechart.HistoryState#getLastState <em>Last State</em>}</li>
 * </ul>
 *
 * @see edan70.statechart.StatechartPackage#getHistoryState()
 * @model
 * @generated
 */
public interface HistoryState extends State {
	/**
	 * Returns the value of the '<em><b>Last State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last State</em>' reference.
	 * @see #setLastState(State)
	 * @see edan70.statechart.StatechartPackage#getHistoryState_LastState()
	 * @model
	 * @generated
	 */
	State getLastState();

	/**
	 * Sets the value of the '{@link edan70.statechart.HistoryState#getLastState <em>Last State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last State</em>' reference.
	 * @see #getLastState()
	 * @generated
	 */
	void setLastState(State value);

} // HistoryState
