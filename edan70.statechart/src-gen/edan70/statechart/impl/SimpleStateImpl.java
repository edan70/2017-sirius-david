/**
 */
package edan70.statechart.impl;

import edan70.statechart.SimpleState;
import edan70.statechart.StatechartPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simple State</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SimpleStateImpl extends StateImpl implements SimpleState {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimpleStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatechartPackage.Literals.SIMPLE_STATE;
	}

} //SimpleStateImpl
