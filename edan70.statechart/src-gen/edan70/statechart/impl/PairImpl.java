/**
 */
package edan70.statechart.impl;

import edan70.statechart.Pair;
import edan70.statechart.State;
import edan70.statechart.StatechartPackage;
import edan70.statechart.TypeInstance;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Pair</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edan70.statechart.impl.PairImpl#getTypeinstance <em>Typeinstance</em>}</li>
 *   <li>{@link edan70.statechart.impl.PairImpl#getState <em>State</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PairImpl extends MinimalEObjectImpl.Container implements Pair {
	/**
	 * The cached value of the '{@link #getTypeinstance() <em>Typeinstance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeinstance()
	 * @generated
	 * @ordered
	 */
	protected TypeInstance typeinstance;

	/**
	 * The cached value of the '{@link #getState() <em>State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected State state;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PairImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatechartPackage.Literals.PAIR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeInstance getTypeinstance() {
		if (typeinstance != null && typeinstance.eIsProxy()) {
			InternalEObject oldTypeinstance = (InternalEObject) typeinstance;
			typeinstance = (TypeInstance) eResolveProxy(oldTypeinstance);
			if (typeinstance != oldTypeinstance) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatechartPackage.PAIR__TYPEINSTANCE,
							oldTypeinstance, typeinstance));
			}
		}
		return typeinstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeInstance basicGetTypeinstance() {
		return typeinstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypeinstance(TypeInstance newTypeinstance) {
		TypeInstance oldTypeinstance = typeinstance;
		typeinstance = newTypeinstance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatechartPackage.PAIR__TYPEINSTANCE, oldTypeinstance,
					typeinstance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getState() {
		if (state != null && state.eIsProxy()) {
			InternalEObject oldState = (InternalEObject) state;
			state = (State) eResolveProxy(oldState);
			if (state != oldState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatechartPackage.PAIR__STATE, oldState,
							state));
			}
		}
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetState() {
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setState(State newState) {
		State oldState = state;
		state = newState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatechartPackage.PAIR__STATE, oldState, state));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case StatechartPackage.PAIR__TYPEINSTANCE:
			if (resolve)
				return getTypeinstance();
			return basicGetTypeinstance();
		case StatechartPackage.PAIR__STATE:
			if (resolve)
				return getState();
			return basicGetState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case StatechartPackage.PAIR__TYPEINSTANCE:
			setTypeinstance((TypeInstance) newValue);
			return;
		case StatechartPackage.PAIR__STATE:
			setState((State) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case StatechartPackage.PAIR__TYPEINSTANCE:
			setTypeinstance((TypeInstance) null);
			return;
		case StatechartPackage.PAIR__STATE:
			setState((State) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case StatechartPackage.PAIR__TYPEINSTANCE:
			return typeinstance != null;
		case StatechartPackage.PAIR__STATE:
			return state != null;
		}
		return super.eIsSet(featureID);
	}

} //PairImpl
