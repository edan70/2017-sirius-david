/**
 */
package edan70.statechart.impl;

import edan70.statechart.SimpleEvent;
import edan70.statechart.StatechartPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simple Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SimpleEventImpl extends EventImpl implements SimpleEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimpleEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatechartPackage.Literals.SIMPLE_EVENT;
	}

} //SimpleEventImpl
