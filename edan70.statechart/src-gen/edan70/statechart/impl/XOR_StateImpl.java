/**
 */
package edan70.statechart.impl;

import edan70.statechart.HistoryState;
import edan70.statechart.State;
import edan70.statechart.StatechartPackage;
import edan70.statechart.XOR_State;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XOR State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edan70.statechart.impl.XOR_StateImpl#getStartState <em>Start State</em>}</li>
 *   <li>{@link edan70.statechart.impl.XOR_StateImpl#getHistoryState <em>History State</em>}</li>
 * </ul>
 *
 * @generated
 */
public class XOR_StateImpl extends StateImpl implements XOR_State {
	/**
	 * The cached value of the '{@link #getStartState() <em>Start State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartState()
	 * @generated
	 * @ordered
	 */
	protected State startState;

	/**
	 * The cached value of the '{@link #getHistoryState() <em>History State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistoryState()
	 * @generated
	 * @ordered
	 */
	protected HistoryState historyState;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XOR_StateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatechartPackage.Literals.XOR_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getStartState() {
		if (startState != null && startState.eIsProxy()) {
			InternalEObject oldStartState = (InternalEObject) startState;
			startState = (State) eResolveProxy(oldStartState);
			if (startState != oldStartState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatechartPackage.XOR_STATE__START_STATE,
							oldStartState, startState));
			}
		}
		return startState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetStartState() {
		return startState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartState(State newStartState) {
		State oldStartState = startState;
		startState = newStartState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatechartPackage.XOR_STATE__START_STATE,
					oldStartState, startState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HistoryState getHistoryState() {
		if (historyState != null && historyState.eIsProxy()) {
			InternalEObject oldHistoryState = (InternalEObject) historyState;
			historyState = (HistoryState) eResolveProxy(oldHistoryState);
			if (historyState != oldHistoryState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							StatechartPackage.XOR_STATE__HISTORY_STATE, oldHistoryState, historyState));
			}
		}
		return historyState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HistoryState basicGetHistoryState() {
		return historyState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHistoryState(HistoryState newHistoryState) {
		HistoryState oldHistoryState = historyState;
		historyState = newHistoryState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatechartPackage.XOR_STATE__HISTORY_STATE,
					oldHistoryState, historyState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case StatechartPackage.XOR_STATE__START_STATE:
			if (resolve)
				return getStartState();
			return basicGetStartState();
		case StatechartPackage.XOR_STATE__HISTORY_STATE:
			if (resolve)
				return getHistoryState();
			return basicGetHistoryState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case StatechartPackage.XOR_STATE__START_STATE:
			setStartState((State) newValue);
			return;
		case StatechartPackage.XOR_STATE__HISTORY_STATE:
			setHistoryState((HistoryState) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case StatechartPackage.XOR_STATE__START_STATE:
			setStartState((State) null);
			return;
		case StatechartPackage.XOR_STATE__HISTORY_STATE:
			setHistoryState((HistoryState) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case StatechartPackage.XOR_STATE__START_STATE:
			return startState != null;
		case StatechartPackage.XOR_STATE__HISTORY_STATE:
			return historyState != null;
		}
		return super.eIsSet(featureID);
	}

} //XOR_StateImpl
