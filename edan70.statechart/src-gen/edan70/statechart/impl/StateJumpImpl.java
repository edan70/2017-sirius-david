/**
 */
package edan70.statechart.impl;

import edan70.statechart.Event;
import edan70.statechart.State;
import edan70.statechart.StateJump;
import edan70.statechart.StatechartPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>State Jump</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edan70.statechart.impl.StateJumpImpl#getFirst <em>First</em>}</li>
 *   <li>{@link edan70.statechart.impl.StateJumpImpl#getEvents <em>Events</em>}</li>
 *   <li>{@link edan70.statechart.impl.StateJumpImpl#getSecond <em>Second</em>}</li>
 *   <li>{@link edan70.statechart.impl.StateJumpImpl#isSimulating <em>Simulating</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StateJumpImpl extends MinimalEObjectImpl.Container implements StateJump {
	/**
	 * The cached value of the '{@link #getFirst() <em>First</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirst()
	 * @generated
	 * @ordered
	 */
	protected State first;

	/**
	 * The cached value of the '{@link #getEvents() <em>Events</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> events;

	/**
	 * The cached value of the '{@link #getSecond() <em>Second</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSecond()
	 * @generated
	 * @ordered
	 */
	protected State second;

	/**
	 * The default value of the '{@link #isSimulating() <em>Simulating</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSimulating()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SIMULATING_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSimulating() <em>Simulating</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSimulating()
	 * @generated
	 * @ordered
	 */
	protected boolean simulating = SIMULATING_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StateJumpImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatechartPackage.Literals.STATE_JUMP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getFirst() {
		if (first != null && first.eIsProxy()) {
			InternalEObject oldFirst = (InternalEObject) first;
			first = (State) eResolveProxy(oldFirst);
			if (first != oldFirst) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatechartPackage.STATE_JUMP__FIRST,
							oldFirst, first));
			}
		}
		return first;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetFirst() {
		return first;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirst(State newFirst) {
		State oldFirst = first;
		first = newFirst;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatechartPackage.STATE_JUMP__FIRST, oldFirst,
					first));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getEvents() {
		if (events == null) {
			events = new EObjectResolvingEList<Event>(Event.class, this, StatechartPackage.STATE_JUMP__EVENTS);
		}
		return events;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getSecond() {
		if (second != null && second.eIsProxy()) {
			InternalEObject oldSecond = (InternalEObject) second;
			second = (State) eResolveProxy(oldSecond);
			if (second != oldSecond) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatechartPackage.STATE_JUMP__SECOND,
							oldSecond, second));
			}
		}
		return second;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetSecond() {
		return second;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSecond(State newSecond) {
		State oldSecond = second;
		second = newSecond;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatechartPackage.STATE_JUMP__SECOND, oldSecond,
					second));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSimulating() {
		return simulating;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSimulating(boolean newSimulating) {
		boolean oldSimulating = simulating;
		simulating = newSimulating;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatechartPackage.STATE_JUMP__SIMULATING,
					oldSimulating, simulating));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case StatechartPackage.STATE_JUMP__FIRST:
			if (resolve)
				return getFirst();
			return basicGetFirst();
		case StatechartPackage.STATE_JUMP__EVENTS:
			return getEvents();
		case StatechartPackage.STATE_JUMP__SECOND:
			if (resolve)
				return getSecond();
			return basicGetSecond();
		case StatechartPackage.STATE_JUMP__SIMULATING:
			return isSimulating();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case StatechartPackage.STATE_JUMP__FIRST:
			setFirst((State) newValue);
			return;
		case StatechartPackage.STATE_JUMP__EVENTS:
			getEvents().clear();
			getEvents().addAll((Collection<? extends Event>) newValue);
			return;
		case StatechartPackage.STATE_JUMP__SECOND:
			setSecond((State) newValue);
			return;
		case StatechartPackage.STATE_JUMP__SIMULATING:
			setSimulating((Boolean) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case StatechartPackage.STATE_JUMP__FIRST:
			setFirst((State) null);
			return;
		case StatechartPackage.STATE_JUMP__EVENTS:
			getEvents().clear();
			return;
		case StatechartPackage.STATE_JUMP__SECOND:
			setSecond((State) null);
			return;
		case StatechartPackage.STATE_JUMP__SIMULATING:
			setSimulating(SIMULATING_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case StatechartPackage.STATE_JUMP__FIRST:
			return first != null;
		case StatechartPackage.STATE_JUMP__EVENTS:
			return events != null && !events.isEmpty();
		case StatechartPackage.STATE_JUMP__SECOND:
			return second != null;
		case StatechartPackage.STATE_JUMP__SIMULATING:
			return simulating != SIMULATING_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (simulating: ");
		result.append(simulating);
		result.append(')');
		return result.toString();
	}

} //StateJumpImpl
