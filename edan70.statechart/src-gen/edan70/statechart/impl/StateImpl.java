/**
 */
package edan70.statechart.impl;

import edan70.statechart.Pair;
import edan70.statechart.State;
import edan70.statechart.StateJump;
import edan70.statechart.StatechartPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edan70.statechart.impl.StateImpl#getName <em>Name</em>}</li>
 *   <li>{@link edan70.statechart.impl.StateImpl#getStates <em>States</em>}</li>
 *   <li>{@link edan70.statechart.impl.StateImpl#getStatejumps <em>Statejumps</em>}</li>
 *   <li>{@link edan70.statechart.impl.StateImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link edan70.statechart.impl.StateImpl#isSimulating <em>Simulating</em>}</li>
 *   <li>{@link edan70.statechart.impl.StateImpl#isStart <em>Start</em>}</li>
 *   <li>{@link edan70.statechart.impl.StateImpl#getClonedStates <em>Cloned States</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class StateImpl extends MinimalEObjectImpl.Container implements State {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStates() <em>States</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStates()
	 * @generated
	 * @ordered
	 */
	protected EList<State> states;

	/**
	 * The cached value of the '{@link #getStatejumps() <em>Statejumps</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatejumps()
	 * @generated
	 * @ordered
	 */
	protected EList<StateJump> statejumps;

	/**
	 * The cached value of the '{@link #getParent() <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent()
	 * @generated
	 * @ordered
	 */
	protected State parent;

	/**
	 * The default value of the '{@link #isSimulating() <em>Simulating</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSimulating()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SIMULATING_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSimulating() <em>Simulating</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSimulating()
	 * @generated
	 * @ordered
	 */
	protected boolean simulating = SIMULATING_EDEFAULT;

	/**
	 * The default value of the '{@link #isStart() <em>Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isStart()
	 * @generated
	 * @ordered
	 */
	protected static final boolean START_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isStart() <em>Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isStart()
	 * @generated
	 * @ordered
	 */
	protected boolean start = START_EDEFAULT;

	/**
	 * The cached value of the '{@link #getClonedStates() <em>Cloned States</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClonedStates()
	 * @generated
	 * @ordered
	 */
	protected EList<Pair> clonedStates;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatechartPackage.Literals.STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatechartPackage.STATE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<State> getStates() {
		if (states == null) {
			states = new EObjectContainmentEList<State>(State.class, this, StatechartPackage.STATE__STATES);
		}
		return states;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StateJump> getStatejumps() {
		if (statejumps == null) {
			statejumps = new EObjectContainmentEList<StateJump>(StateJump.class, this,
					StatechartPackage.STATE__STATEJUMPS);
		}
		return statejumps;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getParent() {
		if (parent != null && parent.eIsProxy()) {
			InternalEObject oldParent = (InternalEObject) parent;
			parent = (State) eResolveProxy(oldParent);
			if (parent != oldParent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatechartPackage.STATE__PARENT,
							oldParent, parent));
			}
		}
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetParent() {
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(State newParent) {
		State oldParent = parent;
		parent = newParent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatechartPackage.STATE__PARENT, oldParent, parent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSimulating() {
		return simulating;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSimulating(boolean newSimulating) {
		boolean oldSimulating = simulating;
		simulating = newSimulating;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatechartPackage.STATE__SIMULATING, oldSimulating,
					simulating));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isStart() {
		return start;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStart(boolean newStart) {
		boolean oldStart = start;
		start = newStart;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatechartPackage.STATE__START, oldStart, start));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Pair> getClonedStates() {
		if (clonedStates == null) {
			clonedStates = new EObjectContainmentEList<Pair>(Pair.class, this, StatechartPackage.STATE__CLONED_STATES);
		}
		return clonedStates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case StatechartPackage.STATE__STATES:
			return ((InternalEList<?>) getStates()).basicRemove(otherEnd, msgs);
		case StatechartPackage.STATE__STATEJUMPS:
			return ((InternalEList<?>) getStatejumps()).basicRemove(otherEnd, msgs);
		case StatechartPackage.STATE__CLONED_STATES:
			return ((InternalEList<?>) getClonedStates()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case StatechartPackage.STATE__NAME:
			return getName();
		case StatechartPackage.STATE__STATES:
			return getStates();
		case StatechartPackage.STATE__STATEJUMPS:
			return getStatejumps();
		case StatechartPackage.STATE__PARENT:
			if (resolve)
				return getParent();
			return basicGetParent();
		case StatechartPackage.STATE__SIMULATING:
			return isSimulating();
		case StatechartPackage.STATE__START:
			return isStart();
		case StatechartPackage.STATE__CLONED_STATES:
			return getClonedStates();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case StatechartPackage.STATE__NAME:
			setName((String) newValue);
			return;
		case StatechartPackage.STATE__STATES:
			getStates().clear();
			getStates().addAll((Collection<? extends State>) newValue);
			return;
		case StatechartPackage.STATE__STATEJUMPS:
			getStatejumps().clear();
			getStatejumps().addAll((Collection<? extends StateJump>) newValue);
			return;
		case StatechartPackage.STATE__PARENT:
			setParent((State) newValue);
			return;
		case StatechartPackage.STATE__SIMULATING:
			setSimulating((Boolean) newValue);
			return;
		case StatechartPackage.STATE__START:
			setStart((Boolean) newValue);
			return;
		case StatechartPackage.STATE__CLONED_STATES:
			getClonedStates().clear();
			getClonedStates().addAll((Collection<? extends Pair>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case StatechartPackage.STATE__NAME:
			setName(NAME_EDEFAULT);
			return;
		case StatechartPackage.STATE__STATES:
			getStates().clear();
			return;
		case StatechartPackage.STATE__STATEJUMPS:
			getStatejumps().clear();
			return;
		case StatechartPackage.STATE__PARENT:
			setParent((State) null);
			return;
		case StatechartPackage.STATE__SIMULATING:
			setSimulating(SIMULATING_EDEFAULT);
			return;
		case StatechartPackage.STATE__START:
			setStart(START_EDEFAULT);
			return;
		case StatechartPackage.STATE__CLONED_STATES:
			getClonedStates().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case StatechartPackage.STATE__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case StatechartPackage.STATE__STATES:
			return states != null && !states.isEmpty();
		case StatechartPackage.STATE__STATEJUMPS:
			return statejumps != null && !statejumps.isEmpty();
		case StatechartPackage.STATE__PARENT:
			return parent != null;
		case StatechartPackage.STATE__SIMULATING:
			return simulating != SIMULATING_EDEFAULT;
		case StatechartPackage.STATE__START:
			return start != START_EDEFAULT;
		case StatechartPackage.STATE__CLONED_STATES:
			return clonedStates != null && !clonedStates.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", simulating: ");
		result.append(simulating);
		result.append(", start: ");
		result.append(start);
		result.append(')');
		return result.toString();
	}

} //StateImpl
