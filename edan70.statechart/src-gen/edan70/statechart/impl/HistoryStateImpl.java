/**
 */
package edan70.statechart.impl;

import edan70.statechart.HistoryState;
import edan70.statechart.State;
import edan70.statechart.StatechartPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>History State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edan70.statechart.impl.HistoryStateImpl#getLastState <em>Last State</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HistoryStateImpl extends StateImpl implements HistoryState {
	/**
	 * The cached value of the '{@link #getLastState() <em>Last State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastState()
	 * @generated
	 * @ordered
	 */
	protected State lastState;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HistoryStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatechartPackage.Literals.HISTORY_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getLastState() {
		if (lastState != null && lastState.eIsProxy()) {
			InternalEObject oldLastState = (InternalEObject) lastState;
			lastState = (State) eResolveProxy(oldLastState);
			if (lastState != oldLastState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							StatechartPackage.HISTORY_STATE__LAST_STATE, oldLastState, lastState));
			}
		}
		return lastState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetLastState() {
		return lastState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastState(State newLastState) {
		State oldLastState = lastState;
		lastState = newLastState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatechartPackage.HISTORY_STATE__LAST_STATE,
					oldLastState, lastState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case StatechartPackage.HISTORY_STATE__LAST_STATE:
			if (resolve)
				return getLastState();
			return basicGetLastState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case StatechartPackage.HISTORY_STATE__LAST_STATE:
			setLastState((State) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case StatechartPackage.HISTORY_STATE__LAST_STATE:
			setLastState((State) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case StatechartPackage.HISTORY_STATE__LAST_STATE:
			return lastState != null;
		}
		return super.eIsSet(featureID);
	}

} //HistoryStateImpl
