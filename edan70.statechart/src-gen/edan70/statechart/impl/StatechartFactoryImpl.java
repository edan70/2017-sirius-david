/**
 */
package edan70.statechart.impl;

import edan70.statechart.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StatechartFactoryImpl extends EFactoryImpl implements StatechartFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static StatechartFactory init() {
		try {
			StatechartFactory theStatechartFactory = (StatechartFactory) EPackage.Registry.INSTANCE
					.getEFactory(StatechartPackage.eNS_URI);
			if (theStatechartFactory != null) {
				return theStatechartFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new StatechartFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatechartFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case StatechartPackage.STATE_JUMP:
			return createStateJump();
		case StatechartPackage.CONDITIONAL_EVENT:
			return createConditionalEvent();
		case StatechartPackage.PROGRAM:
			return createProgram();
		case StatechartPackage.SIMPLE_STATE:
			return createSimpleState();
		case StatechartPackage.XOR_STATE:
			return createXOR_State();
		case StatechartPackage.AND_STATE:
			return createAND_State();
		case StatechartPackage.SIMPLE_EVENT:
			return createSimpleEvent();
		case StatechartPackage.TYPE:
			return createType();
		case StatechartPackage.TYPE_INSTANCE:
			return createTypeInstance();
		case StatechartPackage.TYPE_INSTANCE_SHARE:
			return createTypeInstanceShare();
		case StatechartPackage.PERIODIC_EVENT:
			return createPeriodicEvent();
		case StatechartPackage.HISTORY_STATE:
			return createHistoryState();
		case StatechartPackage.PAIR:
			return createPair();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateJump createStateJump() {
		StateJumpImpl stateJump = new StateJumpImpl();
		return stateJump;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionalEvent createConditionalEvent() {
		ConditionalEventImpl conditionalEvent = new ConditionalEventImpl();
		return conditionalEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Program createProgram() {
		ProgramImpl program = new ProgramImpl();
		return program;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleState createSimpleState() {
		SimpleStateImpl simpleState = new SimpleStateImpl();
		return simpleState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XOR_State createXOR_State() {
		XOR_StateImpl xoR_State = new XOR_StateImpl();
		return xoR_State;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AND_State createAND_State() {
		AND_StateImpl anD_State = new AND_StateImpl();
		return anD_State;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleEvent createSimpleEvent() {
		SimpleEventImpl simpleEvent = new SimpleEventImpl();
		return simpleEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type createType() {
		TypeImpl type = new TypeImpl();
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeInstance createTypeInstance() {
		TypeInstanceImpl typeInstance = new TypeInstanceImpl();
		return typeInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeInstanceShare createTypeInstanceShare() {
		TypeInstanceShareImpl typeInstanceShare = new TypeInstanceShareImpl();
		return typeInstanceShare;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PeriodicEvent createPeriodicEvent() {
		PeriodicEventImpl periodicEvent = new PeriodicEventImpl();
		return periodicEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HistoryState createHistoryState() {
		HistoryStateImpl historyState = new HistoryStateImpl();
		return historyState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Pair createPair() {
		PairImpl pair = new PairImpl();
		return pair;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatechartPackage getStatechartPackage() {
		return (StatechartPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static StatechartPackage getPackage() {
		return StatechartPackage.eINSTANCE;
	}

} //StatechartFactoryImpl
