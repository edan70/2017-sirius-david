/**
 */
package edan70.statechart;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XOR State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edan70.statechart.XOR_State#getStartState <em>Start State</em>}</li>
 *   <li>{@link edan70.statechart.XOR_State#getHistoryState <em>History State</em>}</li>
 * </ul>
 *
 * @see edan70.statechart.StatechartPackage#getXOR_State()
 * @model
 * @generated
 */
public interface XOR_State extends State {
	/**
	 * Returns the value of the '<em><b>Start State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start State</em>' reference.
	 * @see #setStartState(State)
	 * @see edan70.statechart.StatechartPackage#getXOR_State_StartState()
	 * @model required="true"
	 * @generated
	 */
	State getStartState();

	/**
	 * Sets the value of the '{@link edan70.statechart.XOR_State#getStartState <em>Start State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start State</em>' reference.
	 * @see #getStartState()
	 * @generated
	 */
	void setStartState(State value);

	/**
	 * Returns the value of the '<em><b>History State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>History State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>History State</em>' reference.
	 * @see #setHistoryState(HistoryState)
	 * @see edan70.statechart.StatechartPackage#getXOR_State_HistoryState()
	 * @model
	 * @generated
	 */
	HistoryState getHistoryState();

	/**
	 * Sets the value of the '{@link edan70.statechart.XOR_State#getHistoryState <em>History State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>History State</em>' reference.
	 * @see #getHistoryState()
	 * @generated
	 */
	void setHistoryState(HistoryState value);

} // XOR_State
