/**
 */
package edan70.statechart;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edan70.statechart.State#getName <em>Name</em>}</li>
 *   <li>{@link edan70.statechart.State#getStates <em>States</em>}</li>
 *   <li>{@link edan70.statechart.State#getStatejumps <em>Statejumps</em>}</li>
 *   <li>{@link edan70.statechart.State#getParent <em>Parent</em>}</li>
 *   <li>{@link edan70.statechart.State#isSimulating <em>Simulating</em>}</li>
 *   <li>{@link edan70.statechart.State#isStart <em>Start</em>}</li>
 *   <li>{@link edan70.statechart.State#getClonedStates <em>Cloned States</em>}</li>
 * </ul>
 *
 * @see edan70.statechart.StatechartPackage#getState()
 * @model abstract="true"
 * @generated
 */
public interface State extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see edan70.statechart.StatechartPackage#getState_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link edan70.statechart.State#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>States</b></em>' containment reference list.
	 * The list contents are of type {@link edan70.statechart.State}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>States</em>' containment reference list.
	 * @see edan70.statechart.StatechartPackage#getState_States()
	 * @model containment="true"
	 * @generated
	 */
	EList<State> getStates();

	/**
	 * Returns the value of the '<em><b>Statejumps</b></em>' containment reference list.
	 * The list contents are of type {@link edan70.statechart.StateJump}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Statejumps</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statejumps</em>' containment reference list.
	 * @see edan70.statechart.StatechartPackage#getState_Statejumps()
	 * @model containment="true"
	 * @generated
	 */
	EList<StateJump> getStatejumps();

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference.
	 * @see #setParent(State)
	 * @see edan70.statechart.StatechartPackage#getState_Parent()
	 * @model
	 * @generated
	 */
	State getParent();

	/**
	 * Sets the value of the '{@link edan70.statechart.State#getParent <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(State value);

	/**
	 * Returns the value of the '<em><b>Simulating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simulating</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simulating</em>' attribute.
	 * @see #setSimulating(boolean)
	 * @see edan70.statechart.StatechartPackage#getState_Simulating()
	 * @model required="true"
	 * @generated
	 */
	boolean isSimulating();

	/**
	 * Sets the value of the '{@link edan70.statechart.State#isSimulating <em>Simulating</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Simulating</em>' attribute.
	 * @see #isSimulating()
	 * @generated
	 */
	void setSimulating(boolean value);

	/**
	 * Returns the value of the '<em><b>Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start</em>' attribute.
	 * @see #setStart(boolean)
	 * @see edan70.statechart.StatechartPackage#getState_Start()
	 * @model required="true"
	 * @generated
	 */
	boolean isStart();

	/**
	 * Sets the value of the '{@link edan70.statechart.State#isStart <em>Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start</em>' attribute.
	 * @see #isStart()
	 * @generated
	 */
	void setStart(boolean value);

	/**
	 * Returns the value of the '<em><b>Cloned States</b></em>' containment reference list.
	 * The list contents are of type {@link edan70.statechart.Pair}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cloned States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cloned States</em>' containment reference list.
	 * @see edan70.statechart.StatechartPackage#getState_ClonedStates()
	 * @model containment="true"
	 * @generated
	 */
	EList<Pair> getClonedStates();

} // State
