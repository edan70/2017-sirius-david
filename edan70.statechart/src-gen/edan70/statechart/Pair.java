/**
 */
package edan70.statechart;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pair</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edan70.statechart.Pair#getTypeinstance <em>Typeinstance</em>}</li>
 *   <li>{@link edan70.statechart.Pair#getState <em>State</em>}</li>
 * </ul>
 *
 * @see edan70.statechart.StatechartPackage#getPair()
 * @model
 * @generated
 */
public interface Pair extends EObject {
	/**
	 * Returns the value of the '<em><b>Typeinstance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Typeinstance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Typeinstance</em>' reference.
	 * @see #setTypeinstance(TypeInstance)
	 * @see edan70.statechart.StatechartPackage#getPair_Typeinstance()
	 * @model
	 * @generated
	 */
	TypeInstance getTypeinstance();

	/**
	 * Sets the value of the '{@link edan70.statechart.Pair#getTypeinstance <em>Typeinstance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Typeinstance</em>' reference.
	 * @see #getTypeinstance()
	 * @generated
	 */
	void setTypeinstance(TypeInstance value);

	/**
	 * Returns the value of the '<em><b>State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' reference.
	 * @see #setState(State)
	 * @see edan70.statechart.StatechartPackage#getPair_State()
	 * @model
	 * @generated
	 */
	State getState();

	/**
	 * Sets the value of the '{@link edan70.statechart.Pair#getState <em>State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State</em>' reference.
	 * @see #getState()
	 * @generated
	 */
	void setState(State value);

} // Pair
