/**
 */
package edan70.statechart;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edan70.statechart.StatechartPackage#getSimpleEvent()
 * @model
 * @generated
 */
public interface SimpleEvent extends Event {
} // SimpleEvent
