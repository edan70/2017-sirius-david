/**
 */
package edan70.statechart;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Instance Share</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edan70.statechart.TypeInstanceShare#getReferencedType <em>Referenced Type</em>}</li>
 * </ul>
 *
 * @see edan70.statechart.StatechartPackage#getTypeInstanceShare()
 * @model
 * @generated
 */
public interface TypeInstanceShare extends XOR_State {
	/**
	 * Returns the value of the '<em><b>Referenced Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Type</em>' reference.
	 * @see #setReferencedType(Type)
	 * @see edan70.statechart.StatechartPackage#getTypeInstanceShare_ReferencedType()
	 * @model
	 * @generated
	 */
	Type getReferencedType();

	/**
	 * Sets the value of the '{@link edan70.statechart.TypeInstanceShare#getReferencedType <em>Referenced Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referenced Type</em>' reference.
	 * @see #getReferencedType()
	 * @generated
	 */
	void setReferencedType(Type value);

} // TypeInstanceShare
