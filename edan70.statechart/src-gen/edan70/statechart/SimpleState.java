/**
 */
package edan70.statechart;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple State</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edan70.statechart.StatechartPackage#getSimpleState()
 * @model
 * @generated
 */
public interface SimpleState extends State {
} // SimpleState
