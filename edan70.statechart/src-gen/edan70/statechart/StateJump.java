/**
 */
package edan70.statechart;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State Jump</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edan70.statechart.StateJump#getFirst <em>First</em>}</li>
 *   <li>{@link edan70.statechart.StateJump#getEvents <em>Events</em>}</li>
 *   <li>{@link edan70.statechart.StateJump#getSecond <em>Second</em>}</li>
 *   <li>{@link edan70.statechart.StateJump#isSimulating <em>Simulating</em>}</li>
 * </ul>
 *
 * @see edan70.statechart.StatechartPackage#getStateJump()
 * @model
 * @generated
 */
public interface StateJump extends EObject {
	/**
	 * Returns the value of the '<em><b>First</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First</em>' reference.
	 * @see #setFirst(State)
	 * @see edan70.statechart.StatechartPackage#getStateJump_First()
	 * @model required="true"
	 * @generated
	 */
	State getFirst();

	/**
	 * Sets the value of the '{@link edan70.statechart.StateJump#getFirst <em>First</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First</em>' reference.
	 * @see #getFirst()
	 * @generated
	 */
	void setFirst(State value);

	/**
	 * Returns the value of the '<em><b>Events</b></em>' reference list.
	 * The list contents are of type {@link edan70.statechart.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Events</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events</em>' reference list.
	 * @see edan70.statechart.StatechartPackage#getStateJump_Events()
	 * @model
	 * @generated
	 */
	EList<Event> getEvents();

	/**
	 * Returns the value of the '<em><b>Second</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Second</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Second</em>' reference.
	 * @see #setSecond(State)
	 * @see edan70.statechart.StatechartPackage#getStateJump_Second()
	 * @model required="true"
	 * @generated
	 */
	State getSecond();

	/**
	 * Sets the value of the '{@link edan70.statechart.StateJump#getSecond <em>Second</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Second</em>' reference.
	 * @see #getSecond()
	 * @generated
	 */
	void setSecond(State value);

	/**
	 * Returns the value of the '<em><b>Simulating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simulating</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simulating</em>' attribute.
	 * @see #setSimulating(boolean)
	 * @see edan70.statechart.StatechartPackage#getStateJump_Simulating()
	 * @model required="true"
	 * @generated
	 */
	boolean isSimulating();

	/**
	 * Sets the value of the '{@link edan70.statechart.StateJump#isSimulating <em>Simulating</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Simulating</em>' attribute.
	 * @see #isSimulating()
	 * @generated
	 */
	void setSimulating(boolean value);

} // StateJump
