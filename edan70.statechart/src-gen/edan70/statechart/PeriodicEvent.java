/**
 */
package edan70.statechart;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Periodic Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edan70.statechart.PeriodicEvent#getPeriod <em>Period</em>}</li>
 * </ul>
 *
 * @see edan70.statechart.StatechartPackage#getPeriodicEvent()
 * @model
 * @generated
 */
public interface PeriodicEvent extends Event {
	/**
	 * Returns the value of the '<em><b>Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Period</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Period</em>' attribute.
	 * @see #setPeriod(int)
	 * @see edan70.statechart.StatechartPackage#getPeriodicEvent_Period()
	 * @model
	 * @generated
	 */
	int getPeriod();

	/**
	 * Sets the value of the '{@link edan70.statechart.PeriodicEvent#getPeriod <em>Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Period</em>' attribute.
	 * @see #getPeriod()
	 * @generated
	 */
	void setPeriod(int value);

} // PeriodicEvent
